The post "A spring, a rubber band, and chaos" on https://www.johndcook.com/blog/2020/04/26/spring-rubberband-chaos/ inspired the implementation of the model of L. D. Humphreys and R. Shammas. Finding Unpredictable Behavior in a Simple Ordinary Differential Equation. The College Mathematics Journal, Vol. 31, No. 5 (Nov., 2000), pp. 338-346 in EES Engineering Equation Solver.

The results of the two models are equivalent.
